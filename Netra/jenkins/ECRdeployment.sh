IMAGE_REPO_NAME=test
AWS_DEFAULT_REGION=ap-southeast-1
AWS_ACCOUNT_ID=215227975037


sudo $(aws ecr get-login --no-include-email --region ap-southeast-1)

sudo docker build --network host -t $IMAGE_REPO_NAME -f $DOCKER_FILE .

sudo docker tag $IMAGE_REPO_NAME:latest $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_REPO_NAME:$CI_COMMIT_SHA

sudo docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_REPO_NAME:$CI_COMMIT_SHA