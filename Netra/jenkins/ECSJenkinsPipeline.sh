TASK_FAMILY="POC-TD"

ECS_CLUSTER="POC-Cluster"

SERVICE_NAME="POC-Service"

NEW_IMAGE=475788705971.dkr.ecr.ap-southeast-2.amazonaws.com/thmpoc:$BUILD_NUMBER

AWS_DEFAULT_REGION="ap-southeast-2"

CONTAINER_NAME="POC-Conatiner"

DOCKER_LABLE="test"

HARDLIMITCPU="128"

APPLICATION="AppECS-THM-dev-ecs-cluster-THM-Service"

DEPLOYMENT_GROUP="DgpECS-THM-dev-ecs-cluster-THM-Service"

S3_BUCKET_NAME="thmecscode"


#AWSLOG_STREAM="/var/log/django-gunicorn-ng.loginx"

#AWSLOG_GROUP="abfl-Dev-Blazeclan-ecs-DjangoGunicornNginx-logs"


#CONTAINER_PORT="8080"


NEW_CPU="512"
NEW_MEMORY="256"
Container_CPU=20
Container_MEMORY=256


echo $PWD


sudo docker build  --no-cache --tag 475788705971.dkr.ecr.ap-southeast-2.amazonaws.com/thmpoc:$BUILD_NUMBER .
sudo $(aws ecr get-login --no-include-email --region ap-southeast-2)
sudo docker push 475788705971.dkr.ecr.ap-southeast-2.amazonaws.com/thmpoc:$BUILD_NUMBER

TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition "$TASK_FAMILY" --region "ap-southeast-2")

NEW_TASK_DEFINTIION=$(echo $TASK_DEFINITION | jq --arg IMAGE "$NEW_IMAGE" '.taskDefinition | .containerDefinitions[0].image = $IMAGE | del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities)')


NEW_TASK_INFO=$(aws ecs register-task-definition --region "$AWS_DEFAULT_REGION" --memory "$NEW_MEMORY" --container-definitions "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$NEW_IMAGE\",\"cpu\":$Container_CPU,\"memory\":$Container_MEMORY}]" --cli-input-json "$NEW_TASK_DEFINTIION")

NEW_REVISION=$(echo $NEW_TASK_INFO | jq '.taskDefinition.revision')

echo $NEW_TASK_INFO > output.json
taskDefinitionArn=`cat output.json | jq  '.taskDefinition.taskDefinitionArn' | sed 's/"//g'`

echo $taskDefinitionArn


aws ecs update-service --cluster ${ECS_CLUSTER}  --service ${SERVICE_NAME}  --region "$AWS_DEFAULT_REGION" --task-definition ${TASK_FAMILY}:${NEW_REVISION}


#sed -i "s|taskdefinition|$taskDefinitionArn|g" ./appspec.yaml

#aws s3 cp appspec.yaml s3://thmecscode/

#aws deploy create-deployment --cli-input-json file://create-deployment.json --region "ap-southeast-2"

#/bin/bash -x deploy_script.sh
#rm -f deploy.sh


#aws deploy create-deployment --region $AWS_DEFAULT_REGION --application-name $APPLICATION --deployment-group-name $DEPLOYMENT_GROUP --revision=revisionType="S3",s3Location={bucket=$S3_BUCKET_NAME,key=appspec.yaml,bundleType=zip} --s3-location bucket=$S3_BUCKET_NAME,bundleType=zip,key=appspec.yaml 