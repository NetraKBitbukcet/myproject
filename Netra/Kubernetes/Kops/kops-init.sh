#!/bin/bash
#update linux
#sudo yum update -y
#sudo yum install jq -y
#sudo yum install python-pip -y
#pip install --upgrade awscli -q


#Setup Environment variable:
source ~/.bash_profile


#install kops

curl -LO https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x kops-linux-amd64
sudo mv kops-linux-amd64 /usr/bin/kops
sudo echo "export PATH=$PATH:/usr/bin/kops" >> ~/.bashrc && source ~/.bashrc

#install Kubectl

curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/bin/kubectl
sudo echo "export PATH=$PATH:/usr/bin/kubectl" >> ~/.bashrc && source ~/.bashrc

#Setup IAM Role Manually
#add paths of kops & kubectl 

#Setup s3
aws s3api create-bucket --bucket $s3bucketname --region $awsregion --create-bucket-configuration LocationConstraint=$awsregion
aws s3api put-bucket-versioning --bucket $s3bucketname --versioning-configuration Status=Enabled
aws s3api put-bucket-encryption --bucket $s3bucketname  --server-side-encryption-configuration '{"Rules":[{"ApplyServerSideEncryptionByDefault":{"SSEAlgorithm":"AES256"}}]}'

#Generate key for cluster
##sudo touch ~/.ssh/$clusterkey
#sudo chmod 400 ~/.ssh/$clusterkey
#sudo ssh-keygen -q -t rsa -N '' -f ~/.ssh/$clusterkey 2>/dev/null <<< y >/dev/null
#sudo chmod 400 ~/.ssh/$clusterkey
#sudo chmod 400 ~/.ssh/$clusterkey.pub
