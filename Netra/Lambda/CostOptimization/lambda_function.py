import boto3
import os
import openpyxl
import logging
from datetime import datetime
from datetime import timedelta
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
    ses = boto3.client('ses',region_name='ap-southeast-2')
    wb = openpyxl.Workbook()
    inst = wb.active
    inst.title="ec2_stopped_inst"
    content=['Name','Instance Id','Ip Address','Instance Type','Availability Zone','State Transition Reason','State Reason Message']
    r=1
    c=1
    for item in content:
        cel = inst.cell(row = r, column = c)
        cel.value=item
        c+=1
    ec2 = boto3.client('ec2')
    region=ec2.describe_regions()
    for reg in region['Regions']:
        ec2=boto3.client('ec2',region_name=reg['RegionName'])
        response = ec2.describe_instances()
        for reservation in response["Reservations"]:
            for instance in reservation["Instances"]:
                instance_name=""
                for tag in instance['Tags']:
                    if(tag['Key']=='Name'):
                        instance_name=tag['Value']
                state=instance['State']
                if(state['Name']=='stopped'):
                    reason=instance['StateReason']
                    az=instance['Placement']
                    content=[instance_name,instance['InstanceId'],instance['InstanceType'],az['AvailabilityZone'],instance['StateTransitionReason'],reason['Message']]
                    r+=1
                    c=1
                    for item in content:
                        cel = inst.cell(row = r, column = c)
                        cel.value=item
                        c+=1

    volu=wb.create_sheet(index = 1 , title = "ebs_available_volume")
    content=['Name','Volume Id','Size','Volume Type','Created on','Availability Zone']
    r=1
    c=1
    for item in content:
        cel = volu.cell(row = r, column = c)
        cel.value=item
        c+=1
    region=ec2.describe_regions()
    for reg in region['Regions']:
        ec2=boto3.client('ec2',region_name=reg['RegionName'])
        volumes=ec2.describe_volumes()
        for vol in volumes['Volumes']:
            tags=ec2.describe_tags(Filters=[{'Name': 'resource-id','Values': [vol['VolumeId']]}])
	    name=""
            for tag in tags['Tags']:
                if tag['Key']=='Name':
                    name=tag['Value']
            date=str(vol['CreateTime'])
            if vol['State']=='available':
                content=[name,vol['VolumeId'],vol['Size'],vol['VolumeType'],date,vol['AvailabilityZone']]
                r+=1
                c=1
                for item in content:
                    cel = volu.cell(row = r, column = c)
                    cel.value=item
                    c+=1

    ipp = wb.create_sheet(index = 2 , title = "unused_elastic_ip")
    r=1
    cel=ipp.cell(row = r , column = 1)
    cel.value='Elastic Ip'
    region=ec2.describe_regions()
    for reg in region['Regions']:
        ec2=boto3.client('ec2',region_name=reg['RegionName'])
        elastic_ips=ec2.describe_addresses()
        for ip in elastic_ips['Addresses']:
            info_ip=str(ip)
            result=info_ip.find('NetworkInterfaceId')
            if result<0:
                r+=1
                cel = ipp.cell(row = r , column = 1)
                cel.value=ip['PublicIp']

    snap = wb.create_sheet(index = 3 , title = "snapshot_30days_before")
    content=['Snapshot Id','Size','Created on']
    r=1
    c=1
    for item in content:
        cel = snap.cell(row = r, column = c)
        cel.value=item
        c+=1
    region=ec2.describe_regions()
    for reg in region['Regions']:
        ec2=boto3.client('ec2',region_name=reg['RegionName'])
        acco_no = os.environ['Account_Id']
        snapshots = ec2.describe_snapshots(OwnerIds=[acco_no])
        for snapshot in snapshots['Snapshots']:
            create_date=datetime.date(snapshot['StartTime'])
            date_before_30=(datetime.date(datetime.now())-timedelta(days=30))
            if(create_date <= date_before_30):
                content=[snapshot['SnapshotId'],snapshot['VolumeSize'],snapshot['StartTime']]
                r+=1
                c=1
                for item in content:
                    cel = snap.cell(row = r, column = c)
                    cel.value=item
                    c+=1
    ec2Ami=wb.create_sheet(index = 4 , title = "Ami Attached to EC2")
    content=['Name','Instance Id','ImageId','Availability Zone']
    r=1
    c=1
    for item in content:
        cel = ec2Ami.cell(row = r, column = c)
        cel.value=item
        c+=1
    region=ec2.describe_regions()
    for reg in region['Regions']:
        ec2=boto3.client('ec2',region_name=reg['RegionName'])
        response = ec2.describe_instances()
        for reservation in response["Reservations"]:
            for instance in reservation["Instances"]:
                instance_name=""
                for tag in instance['Tags']:
                    if(tag['Key']=='Name'):
                        instance_name=tag['Value']
                state=instance['State']
                if(state['Name']=='running'):
                    az=instance['Placement']
                    content=[instance_name,instance['InstanceId'],instance['ImageId'],az['AvailabilityZone']]
                    r+=1
                    c=1
                for item in content:
                    cel = ec2Ami.cell(row = r, column = c)
                    cel.value=item
                    c+=1
    LaunchconfigurationAMI = wb.create_sheet(index = 5 , title = "LaunchconfigurationAMI")
    content=['ImageId','state','CreationDate']
    r=1
    c=1
    for item in content:
        cel = LaunchconfigurationAMI.cell(row = r, column = c)
        cel.value=item
        c+=1
    autoscaling = boto3.client('autoscaling')
    region=ec2.describe_regions()
    for reg in region['Regions']:
        ec2=boto3.client('ec2',region_name=reg['RegionName'])
        response = autoscaling.describe_launch_configurations()
        for LaunchConfig in response['LaunchConfigurations']:
            content=[LaunchConfig['ImageId'],LaunchConfig['LaunchConfigurationName']]
            r+=1
            c=1
            for item in content:
                cel = LaunchconfigurationAMI.cell(row = r, column = c)
                cel.value=item
                c+=1
    
    wb.save('/tmp/excell.xlsx')

    msg=MIMEMultipart()
    recipients = ['netra.kalyanshetty@blazeclan.com']
    msg['Subject']=os.environ['Subjectline']
    msg['From']='netra.kalyanshetty@blazeclan.com'
    msg['To'] = ', '.join(recipients)
    part = MIMEText('This is the data of stopped EC2 Instances, available EBS Volumes, unassociated Elastic IP and snapshots before 30days')
    msg.attach(part)
    part= MIMEApplication(open('/tmp/excell.xlsx','rb').read())
    part.add_header('Content-Disposition', 'attachment', filename='Scripts.xlsx')
    msg.attach(part)
    ses.send_raw_email(RawMessage={'Data':msg.as_string()},Source=msg['From'],Destinations=recipients)

