#!/bin/bash
kops version


#Set Environment varaibless
source ~/.bash_profile



#Create Cluster Config

sudo kops create cluster \
--cloud=aws \
--state=${KOPS_STATE_STORE} \
--node-count=2 \
--master-count=3 \
--master-size=${mastertype} \
--node-size=${nodetype} \
--zones=ap-southeast-1a,ap-southeast-1b,ap-southeast-1c \
--master-zones=ap-southeast-1a,ap-southeast-1b,ap-southeast-1c \
--name=${KOPS_CLUSTER_NAME} \
--vpc=${vpcid} \
--subnets=${psubnet1},${psubnet2},${psubnet3} \
--associate-public-ip=false \
--utility-subnets=${utsubnet1},${utsubnet2},${utsubnet3} \
--image=${ami} \
--topology=private \
--networking=amazon-vpc-routed-eni \
--network-cidr=${networkcidr} \
--api-loadbalancer-type=internal \
--ssh-public-key=~/.ssh/${clusterkey}

#Update the cluster
sudo kops get cluster 
sudo kops update cluster --name ${KOPS_CLUSTER_NAME} --state ${KOPS_STATE_STORE} --yes


