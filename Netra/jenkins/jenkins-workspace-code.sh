#!/bin/bash

set -x
set -e

AWS=`which aws`
REGION=$1

IMAGE_SERVER_ID=$2

AMI_DESCRIPTION=$3

#Create new AMI from Image Server

CREATE_IMAGE_OP=`$AWS ec2 create-image --instance-id $IMAGE_SERVER_ID --name $AMI_DESCRIPTION --no-reboot --description $AMI_DESCRIPTION --region $REGION`

if [ $? -ne 0 ]; then
echo "AMI creation failed, aborting..."
exit 1
fi

NEW_IMAGE_ID=`echo $CREATE_IMAGE_OP  | jq -c '.ImageId' | awk -F '"' '{print $2}'`

echo "AMI creation started at:" && date
$AWS ec2 wait image-available --image-ids $NEW_IMAGE_ID --region $REGION
echo "AMI creation finished at:" && date

echo $NEW_IMAGE_ID > ./AMI_ID.txt
