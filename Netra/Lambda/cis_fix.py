def lambda_handler(event, context):

    import time
    from datetime import datetime
    import csv
    import boto3

    iam = boto3.client('iam')
    iam_resource = boto3.resource('iam')

    # Following fuction generated credential report in .csv format

    def get_cred_report():
        x = 0
        status = ""
        while iam.generate_credential_report()['State'] != "COMPLETE":
            time.sleep(2)
            x += 1
            # If no credentail report is delivered within this time fail the check.
            if x > 10:
                status = "Fail: rootUse - no CredentialReport available."
                break
        if "Fail" in status:
            return status
        response = iam.get_credential_report()
        report = []
        reader = csv.DictReader(response['Content'].splitlines(), delimiter=',')
        for row in reader:
            report.append(row)

        # Verify if root key's never been used, if so add N/A
        try:
            if report[0]['access_key_1_last_used_date']:
                pass
        except:
            report[0]['access_key_1_last_used_date'] = "N/A"
        try:
            if report[0]['access_key_2_last_used_date']:
                pass
        except:
            report[0]['access_key_2_last_used_date'] = "N/A"
        return report


    cred_report = get_cred_report()

    # 1.1 Avoid the use of root account - Manual
    # 1.2 Ensure MFA is enabled for all IAM user
    # Users without MFA will be disabled

    for i in range(len(cred_report)):
        last_used = cred_report[i]['password_last_used']

        if cred_report[i]['password_enabled'] == "true" and cred_report[i]['password_last_used'] == 'false':
            user = cred_report[i]['user']
            response = iam.delete_login_profile(
                UserName=user
            )
            print("User "+user+" has lost console access.")

    # 1.3 Ensure credentials unused for 90 days or greater are disabled

    for i in range(len(cred_report)):
        try:
            pass_last_used = str(cred_report[i]['password_last_used'])
            now = time.strftime('%Y-%m-%dT%H:%M:%S+00:00',
                                time.gmtime(time.time()))
            date_format = "%Y-%m-%dT%H:%M:%S+00:00"
            delta = datetime.strptime(
                now, date_format) - datetime.strptime(str(pass_last_used), date_format)
            user = cred_report[i]['user']

            if cred_report[i]['password_enabled'] == "true" and delta.days > 90:
                user = cred_report[i]['user']
                response = iam.delete_login_profile(
                    UserName=user
                )
        except:
            continue

    # 1.4 Ensure access keys are rotated every 90 days or less
    # Following code disables access keys older than 90 days

    for i in range(len(cred_report)):
        username = cred_report[i]['user']
        
        if username == "<root_account>":
            continue
        
        list_user_keys = iam.list_access_keys(UserName=str(username))
        for access_key_metadata in list_user_keys['AccessKeyMetadata']:
            try:
                access_key_id = access_key_metadata['AccessKeyId']
                key_response = iam.get_access_key_last_used(
                    AccessKeyId=access_key_id)
                temp = key_response['AccessKeyLastUsed']
                key_last_used = str(temp['LastUsedDate'])
                now = time.strftime('%Y-%m-%d %H:%M:%S+00:00',
                                    time.gmtime(time.time()))
                date_format = "%Y-%m-%d %H:%M:%S+00:00"
                delta = datetime.strptime(
                    now, date_format) - datetime.strptime(str(key_last_used), date_format)
                if delta.days > 90:
                    access_key = iam.AccessKey(username, access_key_id)
                    deactivate_keys_list = access_key.deactivate()
                    print("Access key for "+username+" has been deactivated.")
            except:
                continue

    # 1.5 Ensure IAM password policy requires at least 1 uppercase letter
    # 1.6 Ensure IAM password policy requires at least 1 lowercase letter
    # 1.7 Ensure IAM password policy requires at least one symbol
    # 1.8 Ensure IAM password policy requires at least one number
    # 1.9 Ensure IAM password policy requires minimum length of 14 or greater
    # 1.10 Ensure IAM password policy prevents password reuse
    # 1.11 Ensure IAM password policy expires passwords within 90 days or less

    account_password_policy = iam_resource.AccountPasswordPolicy()

    response = account_password_policy.update(
        MinimumPasswordLength=14,
        RequireSymbols=True,
        RequireNumbers=True,
        RequireUppercaseCharacters=True,
        RequireLowercaseCharacters=True,
        AllowUsersToChangePassword=True,
        MaxPasswordAge=90,
        PasswordReusePrevention=24,
        HardExpiry=False
    )

    # 1.16 Ensure IAM policies are attached only to groups or roles
    users = iam.list_users()

    for key in users['Users']:
        List_of_Policies = iam.list_attached_user_policies(
            UserName=key['UserName'])
        for a in List_of_Policies['AttachedPolicies']:

            x = key['UserName']
            y = a['PolicyArn']

            response = iam.detach_user_policy(
                UserName=x,
                PolicyArn=y
            )

    # 4.1 Ensure no security groups allow ingress from 0.0.0.0/0 to port 22
    # 4.2 Ensure no security groups allow ingress from 0.0.0.0/0 to port 3389
    client = boto3.client('ec2')

    regions = [region['RegionName']
            for region in client.describe_regions()['Regions']]

    forbidden_ports = [22, 3389]

    for n in regions:
        client = boto3.client('ec2', region_name=n)
        ec2 = boto3.resource('ec2', region_name=n)
        response = client.describe_security_groups()
        for m in response['SecurityGroups']:
            sg = ec2.SecurityGroup(str(m['GroupId']))
            if "0.0.0.0/0" in str(m['IpPermissions']):
                for o in m['IpPermissions']:
                    x = int(o['FromPort'])
                    if (x in forbidden_ports and '0.0.0.0/0' in str(o['IpRanges'])):
                        sg.revoke_ingress(IpPermissions=sg.ip_permissions)


    # 4.3 Ensure the default security group for every VPC restricts all traffic
    for n in regions:
        client = boto3.client('ec2', region_name=n)
        ec2 = boto3.resource('ec2', region_name=n)
        response = client.describe_security_groups(
            Filters=[
                {
                    'Name': 'group-name',
                    'Values': [
                        'default',
                    ]
                },
            ]
        )
        for m in response['SecurityGroups']:
            sg = ec2.SecurityGroup(str(m['GroupId']))
            try:
                sg.revoke_ingress(IpPermissions=sg.ip_permissions)

            except:
                print("Already Fixed: No ingress rule set")
            try:
                sg.revoke_egress(IpPermissions=sg.ip_permissions_egress)

            except:
                print("Already Fixed: No egress rule set")

    # Custom Fix 1: Remove admin roles from EC2 instances
    # This code fetches the complete details of an AWS Account

    role_policy = {}
    list_account_details = iam.get_account_authorization_details(
        Filter=[
            'Role',
        ],

    )
    forbidden_policies = ['AdministratorAccess', 'AmazonEC2FullAccess']
    admin_roles = []
    # This code fetches the Instance roles which have AdministratorAccess Policy in it
    for roles in list_account_details['RoleDetailList']:
        if roles['InstanceProfileList']:
            for policy_list in roles['AttachedManagedPolicies']:
                if policy_list['PolicyName'] in forbidden_policies:
                    temp = roles['Arn']
                    x, y = temp.split('/')
                    admin_roles.append(y)

    while list_account_details['IsTruncated']:
        list_account_details = iam.get_account_authorization_details(
            Filter=[
                'Role',
            ],
            Marker=list_account_details['Marker']
        )

        # This code fetches the Instance roles which have AdministratorAccess Policy in it
        for roles in list_account_details['RoleDetailList']:
            if roles['InstanceProfileList']:
                for policy_list in roles['AttachedManagedPolicies']:
                    if policy_list['PolicyName'] in forbidden_policies:
                        temp = roles['Arn']
                        x, y = temp.split('/')
                        admin_roles.append(y)

    # The following code detaches all the admin roles attached with ec2 in all the regions

    for region in regions:
        conn = boto3.client('ec2', region_name=region)
        response = conn.describe_iam_instance_profile_associations(
            Filters=[
                {
                    'Name': 'state',
                    'Values': [
                        'associated',
                    ]
                },
            ],
            MaxResults=10000
        )
        for instances in response['IamInstanceProfileAssociations']:
            arns = instances['IamInstanceProfile']
            temp = arns['Arn']
            x, y = temp.split('/')
            associationid = instances['AssociationId']
            if y in admin_roles:
                response1 = conn.disassociate_iam_instance_profile(
                    AssociationId=associationid
                )

    # Custom Fix 2: Delete access keys for admin users

    # List user details in AWS account
    list_of_admin_groups = []
    list_of_user_groups = []

    # This code fetches the complete details of an AWS Account
    list_account_details = iam.get_account_authorization_details(
        Filter=[
            'User', 'Group',
        ],
        MaxItems=123,
    )

    # This code fetches the groups which have AdministratorAccess Policy in it
    for groups in list_account_details['GroupDetailList']:
        list_of_groups = groups['AttachedManagedPolicies']
        for group_policy in list_of_groups:
            if group_policy['PolicyName'] == 'AdministratorAccess':
                list_of_admin_groups.append(groups['GroupName'])

    # The below for loop displays the users which are present in group having AdministratorAccess Policy
    for user in list_account_details['UserDetailList']:
        list_of_users = user['UserName']
        # The below for loop displays the groups which have AdministratorAccess Policy attached
        for group in list_of_admin_groups:
            if group in user['GroupList']:
                list_user_keys = iam.list_access_keys(UserName=user['UserName'])
                for access_key_metadata in list_user_keys['AccessKeyMetadata']:
                    access_key_id = access_key_metadata['AccessKeyId']
                    access_key = iam_resource.AccessKey(
                        list_of_users, access_key_id)
                    deactivate_keys_list = access_key.deactivate()
