REGION=us-east-2
IMAGE_SERVER_ID=i-0ba9effad5da6a033
AMI_DESCRIPTION="THM-US-Image-$BUILD_NUMBER"
AutoScalingGroup=THM-Production-APP-LS-ASG
LaunchTemplateName=US-Compute-ASGLSLC
LaunchTemplateId=lt-0951346f2f9443275
InstanceType=m5.large

/bin/bash -x ./image_update.sh $REGION $IMAGE_SERVER_ID $AMI_DESCRIPTION

NEW_AMI_ID=`cat ./AMI_ID.txt`
cat ./AMI_ID.txt



sudo aws ec2 create-launch-template-version --region $REGION --launch-template-id $LaunchTemplateId --source-version '$Latest' --launch-template-data '{"ImageId":"'$NEW_AMI_ID'", "InstanceType":"'$InstanceType'"}'


sudo aws autoscaling update-auto-scaling-group --region $REGION --auto-scaling-group-name $AutoScalingGroup --launch-template LaunchTemplateName=$LaunchTemplateName,Version='$Latest'
