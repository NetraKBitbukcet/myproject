#!/bin/bash
# This script detects platform and architecture, then downloads and installs the matching Deep Security Agent package
 if [[ $(/usr/bin/id -u) -ne 0 ]]; then echo You are not running as the root user.  Please try again with root privileges.;
    logger -t You are not running as the root user.  Please try again with root privileges.;
    exit 1;
 fi;
 if type curl >/dev/null 2>&1; then
  SOURCEURL='https://DSM-ELB-426759559.ap-southeast-1.elb.amazonaws.com:443'
  curl $SOURCEURL/software/deploymentscript/platform/linux/ -o /tmp/DownloadInstallAgentPackage --insecure --silent --tlsv1.2

  if [ -s /tmp/DownloadInstallAgentPackage ]; then
      . /tmp/DownloadInstallAgentPackage
      Download_Install_Agent
  else
     echo "Failed to download the agent installation script."
     logger -t Failed to download the Deep Security Agent installation script
     false
  fi
 else
  echo "Please install CURL before running this script."
  logger -t Please install CURL before running this script
  false
 fi
sleep 15
/opt/ds_agent/dsa_control -r
/opt/ds_agent/dsa_control -a dsm://DSM-ELB-426759559.ap-southeast-1.elb.amazonaws.com:4120/ "tenantID:CD35005A-D636-920C-19B9-15E8D3DB9EEF" "token:C198330D-F8DA-C10A-DFF3-8F00D15701EA" "policyid:11"