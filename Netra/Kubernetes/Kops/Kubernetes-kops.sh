
Install kops:

		curl -LO https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
		chmod +x kops-linux-amd64
		sudo mv kops-linux-amd64 /usr/local/bin/kops

Install kubectl:

		curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
		chmod +x ./kubectl
		sudo mv ./kubectl /usr/local/bin/kubectl

Attach IAM role to instance via CFN

Create S3 bucket:
		aws s3api create-bucket --bucket kubernetes-aws-io-test-1 --create-bucket-configuration LocationConstraint=ap-southeast-2

Enable versioning to s3 bucket:
		aws s3api put-bucket-versioning --bucket kubernetes-aws-io-test-1 --versioning-configuration Status=Enabled

Export the s3 and cluster into variable:
		export KOPS_STATE_STORE=s3://kubernetes-aws-io-test-1
		export KOPS_CLUSTER_NAME=kubecluster.k8s.local
		
Enable s3 encryption:
		aws s3api put-bucket-encryption --bucket kubernetes-aws-io-test-1 --server-side-encryption-configuration '{"Rules":[{"ApplyServerSideEncryptionByDefault":{"SSEAlgorithm":"AES256"}}]}'
		
Create ssh key which will be stored on /home/ec2-user/.ssh/id_rsa
		ssh-keygen
		
Create Cluster config:

kops create cluster \
--cloud=aws \
--state=${KOPS_STATE_STORE} \
--node-count=1 \
--master-count=1 \
--master-size=t2.medium \
--node-size=t2.medium \
--zones=ap-southeast-2a,ap-southeast-2b,ap-southeast-2c \
--name=${KOPS_CLUSTER_NAME} \
--vpc=vpc-07204c89291b05063 \
--subnets=subnet-0f33cb987253a863f,subnet-00f850ed34d598b72,subnet-0cc6d47391ddc212c \
--associate-public-ip=false \
--utility-subnets=subnet-0e52a18a2fac6a13e,subnet-0fbe75b535eaef32a,subnet-05db59dd83ab10fbe \
--image=ami-02a599eb01e3b3c5b \
--topology=private \
--networking=amazon-vpc-routed-eni \
--network-cidr=10.0.0.0/16 \
--api-loadbalancer-type=internal



Create Cluster:

		kops update cluster --name kubecluster.k8s.local --yes