Install git:
	sudo yum install git-all
	git --version
	
Install docker:
	 sudo yum install -y docker
	 sudo service docker start
Create IAM role: Jenkins-CICD-Role

create ECR repo :

$(aws ecr get-login --no-include-email --region ap-southeast-2)


Install jq command:
sudo yum install jq


TASK_FAMILY="POC-TD"
ECS_CLUSTER="ECS-Cluster"
SERVICE_NAME="POC-Service"
NEW_IMAGE=214506135498.dkr.ecr.ap-southeast-2.amazonaws.com/jenkins-cicd-ecr:$BUILD_NUMBER
AWS_DEFAULT_REGION="ap-southeast-2"
CONTAINER_NAME="POC-Conatiner"
NEW_CPU="512"
NEW_MEMORY="256"
Container_CPU=20
Container_MEMORY=256


echo $PWD

#Build the docker image and push the image to the ECR repository

sudo docker build  --no-cache --tag 214506135498.dkr.ecr.ap-southeast-2.amazonaws.com/jenkins-cicd-ecr:$BUILD_NUMBER .
sudo $(aws ecr get-login --no-include-email --region ap-southeast-2)
sudo docker push 214506135498.dkr.ecr.ap-southeast-2.amazonaws.com/jenkins-cicd-ecr:$BUILD_NUMBER

#Fetch the previous Task definition
TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition "$TASK_FAMILY" --region "$AWS_DEFAULT_REGION")

#Echo the previous to the new td with new image
NEW_TASK_DEFINTIION=$(echo $TASK_DEFINITION | jq --arg IMAGE "$NEW_IMAGE" '.taskDefinition | .containerDefinitions[0].image = $IMAGE | del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities)')


#Register TD with the paramertied value
NEW_TASK_INFO=$(aws ecs register-task-definition --region "$AWS_DEFAULT_REGION" --memory "$NEW_MEMORY" --container-definitions "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$NEW_IMAGE\",\"cpu\":$Container_CPU,\"memory\":$Container_MEMORY}]" --cli-input-json "$NEW_TASK_DEFINTIION")

#Create the new revision
NEW_REVISION=$(echo $NEW_TASK_INFO | jq '.taskDefinition.revision')


echo $NEW_TASK_INFO > output.json
taskDefinitionArn=`cat output.json | jq  '.taskDefinition.taskDefinitionArn' | sed 's/"//g'`

echo $taskDefinitionArn

#Update the ECS service
aws ecs update-service --cluster ${ECS_CLUSTER}  --service ${SERVICE_NAME}  --region "$AWS_DEFAULT_REGION" --task-definition ${TASK_FAMILY}:${NEW_REVISION}