---
  #====================================================================================================
  # Description : Configures the billing reports bucket and SNS topic
  # Author      : Itoc - Tim Clydesdale (Modified by Matthew Tobolov)
  # Date        : 12/12/2018
  # Version     : 1.0.0
  #====================================================================================================
  AWSTemplateFormatVersion: '2010-09-09'
  Description: AWS Billing - Budgets - Master Account
  #====================================================================================================
  #                                             Parameters
  #====================================================================================================
  Parameters:
    #--------------------------------------------------------------------------------------------------
    RecipientEmail:
      Default: 'aws.billing@predikt-r.com.au'
      Type: String
      Description: Email recipient for budget alerts.
    #--------------------------------------------------------------------------------------------------
    IdentityAccountId:
      Default: '082257441206'
      AllowedPattern: '^\d{12}$'
      Type: String
      Description: Account Id for the Identity account
    AuditAccountId:
      Default: '530466087767'
      AllowedPattern: '^\d{12}$'
      Type: String
      Description: Account Id for the Audit account
    SharedServicesAccountId:
      Default: '080647778178'
      AllowedPattern: '^\d{12}$'
      Type: String
      Description: Account Id for the Shared Services account
    PreProdAccountId:
      Default: '071302553595'
      AllowedPattern: '^\d{12}$'
      Type: String
      Description: Account Id for the PreProduction account
    ProdAccountId:
      Default: '844656322341'
      AllowedPattern: '^\d{12}$'
      Type: String
      Description: Account Id for the Production account
    DevelopmentAccountId:
      Default: '409269861661'
      AllowedPattern: '^\d{12}$'
      Type: String
      Description: Account Id for the Development account
    #--------------------------------------------------------------------------------------------------
    BillingAccountBudget:
      Type: Number
      Description: Budget for Billing account in USD
      Default: 100
    IdentityAccountBudget:
      Type: Number
      Description: Budget for Identity account in USD
      Default: 100
    AuditAccountBudget:
      Type: Number
      Description: Budget for Audit account in USD
      Default: 100
    SharedServicesAccountBudget:
      Type: Number
      Description: Budget for Shared Services account in USD
      Default: 500
    PreProdAccountBudget:
      Type: Number
      Description: Budget for PreProduction account in USD
      Default: 1000
    ProdAccountBudget:
      Type: Number
      Description: Budget for Production account in USD
      Default: 1000
    DevelopmentAccountBudget:
      Type: Number
      Description: Budget for Development account in USD
      Default: 1000
    #--------------------------------------------------------------------------------------------------
    AlertWhenForecastGreaterThan:
      Type: Number
      Description: Alert when any budget is forecasted to exceed X % of the threshold.
      Default: 100
    AlertWhenActualGreaterThan:
      Type: Number
      Description: Alert when any budget has exceeded X % of the threshold.
      Default: 100
#====================================================================================================
#                                             METADATA
#====================================================================================================
  Metadata:
    AWS::CloudFormation::Interface:
      ParameterGroups:
        - Label:
            default: Budgets - Notification Configuration
          Parameters:
            - AlertWhenForecastGreaterThan
            - AlertWhenActualGreaterThan
        - Label:
            default: Budgets - Billing
          Parameters:
            - BillingAccountBudget
        - Label:
            default: Budgets - Audit
          Parameters:
            - AuditAccountId
            - AuditAccountBudget
        - Label:
            default: Budgets - Identity
          Parameters:
            - IdentityAccountId
            - IdentityAccountBudget
        - Label:
            default: Budgets - Shared Services
          Parameters:
            - SharedServicesAccountId
            - SharedServicesAccountBudget
        - Label:
            default: Budgets - PreProduction
          Parameters:
            - PreProdAccountId
            - PreProdAccountBudget
        - Label:
            default: Budgets - Production
          Parameters:
            - ProdAccountId
            - ProdAccountBudget
        - Label:
            default: Budgets - Development
          Parameters:
            - DevelopmentAccountId
            - DevelopmentAccountBudget
#====================================================================================================
#                                             Resources
#====================================================================================================
  Resources:
    # Configures the SNS Topic
    #--------------------------------------------------------------------------------------------------
    BillingTopic:
      Type: 'AWS::SNS::Topic'
      Properties:
        DisplayName: billing-alerts
        TopicName: billing-alerts
        Subscription:
          - Endpoint: !Ref RecipientEmail
            Protocol: email

    # Configures the SNS Topic Policy
    #--------------------------------------------------------------------------------------------------
    BillingTopicPolicy:
      Type: "AWS::SNS::TopicPolicy"
      Properties:
        PolicyDocument: 
          Version: '2008-10-17'
          Id: Policy_ID
          Statement:
          - Sid: AWSBudgets-notification-1
            Effect: Allow
            Principal:
              Service: budgets.amazonaws.com
            Action: SNS:Publish
            Resource: !Sub arn:aws:sns:ap-southeast-2:${AWS::AccountId}:billing-alerts
        Topics:
          - !Ref BillingTopic
    
    #==================================================================================================
    #                                             Budgets
    #==================================================================================================
    # Configures the Budget for the Master Account
    #--------------------------------------------------------------------------------------------------
    BudgetMaster:
      Type: "AWS::Budgets::Budget"
      Properties:
        Budget:
          BudgetName: Master
          BudgetLimit:
            Amount: !Ref BillingAccountBudget
            Unit: USD
          TimeUnit: MONTHLY
          BudgetType: COST
          CostFilters:
            LinkedAccount:
              - !Ref AWS::AccountId  
        NotificationsWithSubscribers:
          - Notification:
              NotificationType: ACTUAL
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenActualGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic
          - Notification:
              NotificationType: FORECASTED
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenForecastGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic

    # Configures the Budget for the Audit Account
    #--------------------------------------------------------------------------------------------------
    BudgetAudit:
      Type: "AWS::Budgets::Budget"
      Properties:
        Budget:
          BudgetName: Audit
          BudgetLimit:
            Amount: !Ref AuditAccountBudget
            Unit: USD
          TimeUnit: MONTHLY
          BudgetType: COST
          CostFilters:
            LinkedAccount:
              - !Ref AuditAccountId  
        NotificationsWithSubscribers:
          - Notification:
              NotificationType: ACTUAL
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenActualGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic
          - Notification:
              NotificationType: FORECASTED
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenForecastGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic

    # Configures the Budget for the Identity Account
    #--------------------------------------------------------------------------------------------------
    BudgetIdentity:
      Type: "AWS::Budgets::Budget"
      Properties:
        Budget:
          BudgetName: Identity
          BudgetLimit:
            Amount: !Ref IdentityAccountBudget
            Unit: USD
          TimeUnit: MONTHLY
          BudgetType: COST
          CostFilters:
            LinkedAccount:
              - !Ref IdentityAccountId  
        NotificationsWithSubscribers:
          - Notification:
              NotificationType: ACTUAL
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenActualGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic
          - Notification:
              NotificationType: FORECASTED
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenForecastGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic

    # Configures the Budget for the Shared Services Account
    #--------------------------------------------------------------------------------------------------
    BudgetSharedServices:
      Type: "AWS::Budgets::Budget"
      Properties:
        Budget:
          BudgetName: Shared Services
          BudgetLimit:
            Amount: !Ref SharedServicesAccountBudget
            Unit: USD
          TimeUnit: MONTHLY
          BudgetType: COST
          CostFilters:
            LinkedAccount:
              - !Ref SharedServicesAccountId  
        NotificationsWithSubscribers:
          - Notification:
              NotificationType: ACTUAL
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenActualGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic
          - Notification:
              NotificationType: FORECASTED
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenForecastGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic

    # Configures the Budget for the Pre-Production Account
    #--------------------------------------------------------------------------------------------------
    BudgetPreProduction:
      Type: "AWS::Budgets::Budget"
      Properties:
        Budget:
          BudgetName: Pre-Production
          BudgetLimit:
            Amount: !Ref PreProdAccountBudget
            Unit: USD
          TimeUnit: MONTHLY
          BudgetType: COST
          CostFilters:
            LinkedAccount:
              - !Ref PreProdAccountId  
        NotificationsWithSubscribers:
          - Notification:
              NotificationType: ACTUAL
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenActualGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic
          - Notification:
              NotificationType: FORECASTED
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenForecastGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic

    # Configures the Budget for the Production Account
    #--------------------------------------------------------------------------------------------------
    BudgetProduction:
      Type: "AWS::Budgets::Budget"
      Properties:
        Budget:
          BudgetName: Production
          BudgetLimit:
            Amount: !Ref ProdAccountBudget
            Unit: USD
          TimeUnit: MONTHLY
          BudgetType: COST
          CostFilters:
            LinkedAccount:
              - !Ref ProdAccountId  
        NotificationsWithSubscribers:
          - Notification:
              NotificationType: ACTUAL
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenActualGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic
          - Notification:
              NotificationType: FORECASTED
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenForecastGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic

    # Configures the Budget for the Development Account
    #--------------------------------------------------------------------------------------------------
    BudgetDevelopment:
      Type: "AWS::Budgets::Budget"
      Properties:
        Budget:
          BudgetName: Development
          BudgetLimit:
            Amount: !Ref DevelopmentAccountBudget
            Unit: USD
          TimeUnit: MONTHLY
          BudgetType: COST
          CostFilters:
            LinkedAccount:
              - !Ref DevelopmentAccountId  
        NotificationsWithSubscribers:
          - Notification:
              NotificationType: ACTUAL
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenActualGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic
          - Notification:
              NotificationType: FORECASTED
              ComparisonOperator: GREATER_THAN
              Threshold: !Ref AlertWhenForecastGreaterThan
            Subscribers:
            - SubscriptionType: SNS
              Address: !Ref BillingTopic
