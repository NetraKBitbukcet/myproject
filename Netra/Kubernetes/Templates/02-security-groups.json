{
	"AWSTemplateFormatVersion": "2010-09-09",
	"Description": "AWS CFN to create security group for the environment.",
	"Parameters": {
		"ClientName": {
			"Default": "test",
			"Description": "Short & Single Word name of Client. To be used for tagging resources.",
			"Type": "String",
			"MinLength": 1
		},
		"EnvironmentName": {
			"Default": "dev",
			"Description": "To be used for tagging resorces.",
			"Type": "String"
		}
	},
	"Metadata": {
		"AWS::CloudFormation::Interface": {
			"ParameterGroups": [{
				"Label": {
					"default": "Environment Configuration"
				},
				"Parameters": ["ClientName", "EnvironmentName"]
			}]
		}
	},
	"Resources": {
		"BastionSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup",
			"Properties": {
				"GroupDescription": "Allow ssh to client host",
				"VpcId": "vpc-0419c0434e28e45ec",
				"Tags": [{
						"Key": "Name",
						"Value": {
							"Fn::Join": [
								"-", [{
										"Ref": "ClientName"
									},
									{
										"Ref": "EnvironmentName"
									},
									"bastion-sg"
								]
							]
						}
					},
					{
						"Key": "Environment",
						"Value": {
							"Ref": "EnvironmentName"
						}
					},
					{
						"Key": "CreatedBy",
						"Value": "BlazeClan"
					}
				]
			}
		},
		"NodeSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup",
			"Properties": {
				"GroupDescription": "Security group for all nodes in the cluster",
				"VpcId": "vpc-0419c0434e28e45ec",
				"Tags": [{
						"Key": "Name",
						"Value": {
							"Fn::Join": [
								"-", [{
										"Ref": "ClientName"
									},
									{
										"Ref": "EnvironmentName"
									},
									"eksnode-sg"
								]
							]
						}
					},
					{
						"Key": "Environment",
						"Value": {
							"Ref": "EnvironmentName"
						}
					},
					{
						"Key": "CreatedBy",
						"Value": "BlazeClan"
					}
				]

			}
		},
		"ClusterControlPlaneSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup",
			"Properties": {
				"GroupDescription": "Security group for all nodes in the cluster",
				"VpcId": "vpc-0419c0434e28e45ec",
				"Tags": [{
						"Key": "Name",
						"Value": {
							"Fn::Join": [
								"-", [{
										"Ref": "ClientName"
									},
									{
										"Ref": "EnvironmentName"
									},
									"ekscluster-sg"
								]
							]
						}
					},
					{
						"Key": "Environment",
						"Value": {
							"Ref": "EnvironmentName"
						}
					},
					{
						"Key": "CreatedBy",
						"Value": "BlazeClan"
					}
				]

			}
		},
		"DBSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup",
			"Properties": {
				"GroupDescription": "Security group for all RDS DB Instances",
				"VpcId": "vpc-0419c0434e28e45ec",
				"Tags": [{
						"Key": "Name",
						"Value": {
							"Fn::Join": [
								"-", [{
										"Ref": "ClientName"
									},
									{
										"Ref": "EnvironmentName"
									},
									"db-sg"
								]
							]
						}
					},
					{
						"Key": "Environment",
						"Value": {
							"Ref": "EnvironmentName"
						}
					},
					{
						"Key": "CreatedBy",
						"Value": "BlazeClan"
					}
				]

			}
		},
		"LambdaSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup",
			"Properties": {
				"GroupDescription": "Security group for all Lambda",
				"VpcId": "vpc-0419c0434e28e45ec",
				"Tags": [{
						"Key": "Name",
						"Value": {
							"Fn::Join": [
								"-", [{
										"Ref": "ClientName"
									},
									{
										"Ref": "EnvironmentName"
									},
									"lambda-sg"
								]
							]
						}
					},
					{
						"Key": "Environment",
						"Value": {
							"Ref": "EnvironmentName"
						}
					},
					{
						"Key": "CreatedBy",
						"Value": "BlazeClan"
					}
				]

			}
		},
		"LambdaSecurityGroupIngress01": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "LambdaSecurityGroup",
			"Properties": {
				"GroupId": {
					"Ref": "LambdaSecurityGroup"
				},
				"SourceSecurityGroupId": {
					"Ref": "DBSecurityGroup"
				},
				"IpProtocol": "-1"
			}
		},
		"BastionSecurityGroupIngress01": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "BastionSecurityGroup",
			"Properties": {
				"Description": "Blazeclan IP 1",
				"GroupId": {
					"Ref": "BastionSecurityGroup"
				},
				"IpProtocol": "tcp",
				"FromPort": 22,
				"ToPort": 22,
				"CidrIp": "114.143.143.134/32"
			}
		},
		"BastionSecurityGroupIngress02": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "BastionSecurityGroup",
			"Properties": {
				"Description": "Blazeclan IP 2",
				"GroupId": {
					"Ref": "BastionSecurityGroup"
				},
				"IpProtocol": "tcp",
				"FromPort": 22,
				"ToPort": 22,
				"CidrIp": "14.142.23.154/32"
			}
		},
		"NodeSecurityGroupIngress01": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow nodes to communicate with each other",
				"GroupId": {
					"Ref": "NodeSecurityGroup"
				},
				"SourceSecurityGroupId": {
					"Ref": "NodeSecurityGroup"
				},
				"IpProtocol": -1,
				"FromPort": 0,
				"ToPort": 65535
			}
		},
		"NodeSecurityGroupIngress02": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow worker Kubelets and pods to receive communication from the cluster control plane",
				"GroupId": {
					"Ref": "NodeSecurityGroup"
				},
				"SourceSecurityGroupId": {
					"Ref": "ClusterControlPlaneSecurityGroup"
				},
				"IpProtocol": "tcp",
				"FromPort": 1025,
				"ToPort": 65535
			}
		},
		"NodeSecurityGroupIngress03": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow pods running extension API servers on port 443 to receive communication from cluster control plane",
				"GroupId": {
					"Ref": "NodeSecurityGroup"
				},
				"SourceSecurityGroupId": {
					"Ref": "ClusterControlPlaneSecurityGroup"
				},
				"IpProtocol": "tcp",
				"FromPort": 443,
				"ToPort": 443
			}
		},
		"ClusterControlPlaneSecurityGroupIngress01": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow pods to communicate with the cluster API Server",
				"GroupId": {
					"Ref": "ClusterControlPlaneSecurityGroup"
				},
				"SourceSecurityGroupId": {
					"Ref": "NodeSecurityGroup"
				},
				"IpProtocol": "tcp",
				"ToPort": 443,
				"FromPort": 443
			}
		},
		"ClusterControlPlaneSecurityGroupEgress01": {
			"Type": "AWS::EC2::SecurityGroupEgress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow the cluster control plane to communicate with worker Kubelet and pods",
				"GroupId": {
					"Ref": "ClusterControlPlaneSecurityGroup"
				},
				"DestinationSecurityGroupId": {
					"Ref": "NodeSecurityGroup"
				},
				"IpProtocol": "tcp",
				"FromPort": 1025,
				"ToPort": 65535
			}
		},
		"ClusterControlPlaneSecurityGroupEgress02": {
			"Type": "AWS::EC2::SecurityGroupEgress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow the cluster control plane to communicate with pods running extension API servers on port 443",
				"GroupId": {
					"Ref": "ClusterControlPlaneSecurityGroup"
				},
				"DestinationSecurityGroupId": {
					"Ref": "NodeSecurityGroup"
				},
				"IpProtocol": "tcp",
				"FromPort": 443,
				"ToPort": 443
			}
		},
		"DBSecurityGroupIngress01": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow nodes to communicate with the database",
				"GroupId": {
					"Ref": "DBSecurityGroup"
				},
				"SourceSecurityGroupId": {
					"Ref": "NodeSecurityGroup"
				},
				"IpProtocol": "tcp",
				"FromPort": 5432,
				"ToPort": 5432
			}
			
		},
		"DBSecurityGroupIngress02": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow nodes to communicate with the database from bastion",
				"GroupId": {
					"Ref": "DBSecurityGroup"
				},
				"SourceSecurityGroupId": {
					"Ref": "BastionSecurityGroup"
				},
				"IpProtocol": "tcp",
				"FromPort": 5432,
				"ToPort": 5432
			}
			
		},
		"DBSecurityGroupIngress03": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"DependsOn": "NodeSecurityGroup",
			"Properties": {
				"Description": "Allow nodes to communicate with the database from bastion",
				"GroupId": {
					"Ref": "DBSecurityGroup"
				},
				"SourceSecurityGroupId": {
					"Ref": "LambdaSecurityGroup"
				},
				"IpProtocol": "-1"
			}
			
		}

	},
	"Outputs": {
		"BastionSecurityGroupOP": {
			"Description": "Bastion Security group created",
			"Value": {
				"Ref": "BastionSecurityGroup"
			},
			"Export": {
				"Name": {
					"Fn::Join": ["-", [{
						"Ref": "ClientName"
					}, {
						"Ref": "EnvironmentName"
					}, "bastion-sg"]]
				}
			}
		},
		"NodeSecurityGroupOP": {
			"Description": "Node Security group created",
			"Value": {
				"Ref": "NodeSecurityGroup"
			},
			"Export": {
				"Name": {
					"Fn::Join": ["-", [{
						"Ref": "ClientName"
					}, {
						"Ref": "EnvironmentName"
					}, "eksnode-sg"]]
				}
			}
		},
		"ClusterControlPlaneSecurityGroupOP": {
			"Description": "Cluster Security group created",
			"Value": {
				"Ref": "ClusterControlPlaneSecurityGroup"
			},
			"Export": {
				"Name": {
					"Fn::Join": ["-", [{
						"Ref": "ClientName"
					}, {
						"Ref": "EnvironmentName"
					}, "ekscluster-sg"]]
				}
			}
		},
		"DBSecurityGroupOP": {
			"Description": "DB Security group created",
			"Value": {
				"Ref": "DBSecurityGroup"
			},
			"Export": {
				"Name": {
					"Fn::Join": ["-", [{
						"Ref": "ClientName"
					}, {
						"Ref": "EnvironmentName"
					}, "db-sg"]]
				}
			}
		}
	}

}