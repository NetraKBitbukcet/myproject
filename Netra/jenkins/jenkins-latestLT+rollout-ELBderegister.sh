AutoScalingGroup=testasg
LaunchTemplateName=testting
LaunchTemplateId=lt-0919ed7ac8f6869dc
IMAGE_SERVER_ID=i-05e9312f5dc64edc0
InstanceType=t2.micro
REGION=ap-southeast-2
TargetGroupARN=arn:aws:elasticloadbalancing:ap-southeast-2:214506135498:targetgroup/TestTG/75eb91ffa85d7eb4


#AMI_DESCRIPTION="THM-Secure-AU-Image-$BUILD_NUMBER"

#/bin/bash -x ./image_update.sh $REGION $IMAGE_SERVER_ID $AMI_DESCRIPTION

#NEW_AMI_ID=`cat ./AMI_ID.txt`
#cat ./AMI_ID.txt


NEW_AMI_ID=ami-0a58e22c727337c51

sudo aws ec2 create-launch-template-version --region ap-southeast-2 --launch-template-id $LaunchTemplateId --source-version '$Latest' --launch-template-data '{"ImageId":"'$NEW_AMI_ID'", "InstanceType":"'$InstanceType'"}'
sudo aws autoscaling update-auto-scaling-group --region ap-southeast-2 --auto-scaling-group-name $AutoScalingGroup --launch-template LaunchTemplateName=$LaunchTemplateName,Version='$Latest'



if [ "${RollOutLatestImageInProduction}" = "yes" ]
then
	InstanceId=$(aws autoscaling describe-auto-scaling-instances --region ap-southeast-2 --output text \
    --query "AutoScalingInstances[?AutoScalingGroupName=='testasg'].InstanceId" \
    | xargs -n1 aws ec2 describe-instances --instance-ids $ID --region ap-southeast-2 \
    --query "Reservations[].Instances[].InstanceId" --output text)
    
    echo $InstanceId
    
    for instance in $InstanceId 
    do
        AMIID=$(aws ec2 describe-instances --instance-ids $instance --region ap-southeast-2 \
    --query "Reservations[].Instances[].ImageId" --output text)
        echo "for instance $InstanceId is AMI id is  $AMIID"
        
        if [ $AMIID != $NEW_AMI_ID ]
        then
        	aws elbv2 deregister-targets --target-group-arn $TargetGroupARN --targets Id=$instance --region ap-southeast-2
            sleep 100
            aws ec2 terminate-instances --instance-ids $instance --region ap-southeast-2
        else
            echo "None of the condition met"
        fi
        sleep 60
    done
    echo "Ami Updated successfully with rolling out the servers"
else
	echo "Ami Updated successfully without rolling out the servers"
    exit 0
fi


#deregister from elb then wait sleep 500 then terminate













 





