AWS_DEFAULT_REGION=ap-southeast-2
AWS_ACCOUNT_ID=215227975037
EKS_CLUSTER_NAME=xl-axiata-dev-EKSCluster
DeploymentName=
ImageName=
ECR_IMAGE_REPO_NAME=test

aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name $EKS_CLUSTER_NAME

kubectl set image deployment/$DeploymentName $ImageName=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$ECR_IMAGE_REPO_NAME:$CI_COMMIT_SHA

kubectl set image deployment/xl-axiata new-tower=dockercloud/hello-world