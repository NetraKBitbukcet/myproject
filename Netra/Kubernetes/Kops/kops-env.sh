#!/bin/bash
echo "export s3bucketname=amtrust-kops-poc" >> ~/.bash_profile && source ~/.bash_profile
echo "export clientname=amtrust" >> ~/.bash_profile && source ~/.bash_profile
echo "export envname=poc" >> ~/.bash_profile && source ~/.bash_profile
echo "export KOPS_STATE_STORE=s3://$s3bucketname" >> ~/.bash_profile && source ~/.bash_profile 
echo "export KOPS_CLUSTER_NAME=$clientname-$envname-kubecluster.k8s.local" >> ~/.bash_profile && source ~/.bash_profile
echo "export awsregion=ap-southeast-1" >> ~/.bash_profile && source ~/.bash_profile
echo "export clusterkey=amtrust-cluster.pub" >> ~/.bash_profile && source ~/.bash_profile
echo "export networkstackname=AMTRUST-KUBERNETES-NETWORK" >> ~/.bash_profile && source ~/.bash_profile
sudo aws ec2 describe-vpcs --profile jenkins --filters Name=tag:aws:cloudformation:stack-name,Values=$networkstackname --query 'Vpcs[*].VpcId[]' --output text > ~/vpcid
echo "export vpcid=$(cat ~/vpcid)" >> ~/.bash_profile && source ~/.bash_profile
sudo aws ec2 describe-vpcs --profile jenkins --filters Name=tag:aws:cloudformation:stack-name,Values=$networkstackname --query 'Vpcs[*].CidrBlock[]' --output text > ~/vpcidr
sudo aws ec2 describe-subnets --profile jenkins --filters Name=vpc-id,Name=tag:aws:cloudformation:logical-id,Values=$vpcid,Values=PublicSubnetA --query 'Subnets[].[SubnetId]' --output text > ~/psubnet1
sudo aws ec2 describe-subnets --profile jenkins --filters Name=vpc-id,Name=tag:aws:cloudformation:logical-id,Values=$vpcid,Values=PublicSubnetB --query 'Subnets[].[SubnetId]' --output text > ~/psubnet2
sudo aws ec2 describe-subnets --profile jenkins --filters Name=vpc-id,Name=tag:aws:cloudformation:logical-id,Values=$vpcid,Values=PublicSubnetC --query 'Subnets[].[SubnetId]' --output text > ~/psubnet3
sudo aws ec2 describe-subnets --profile jenkins --filters Name=vpc-id,Name=tag:aws:cloudformation:logical-id,Values=$vpcid,Values=PrivateSubnetA --query 'Subnets[].[SubnetId]' --output text > ~/prsubnet1
sudo aws ec2 describe-subnets --profile jenkins --filters Name=vpc-id,Name=tag:aws:cloudformation:logical-id,Values=$vpcid,Values=PrivateSubnetB --query 'Subnets[].[SubnetId]' --output text > ~/prsubnet2
sudo aws ec2 describe-subnets --profile jenkins --filters Name=vpc-id,Name=tag:aws:cloudformation:logical-id,Values=$vpcid,Values=PrivateSubnetC --query 'Subnets[].[SubnetId]' --output text > ~/prsubnet3
echo "export psubnet1=$(cat ~/prsubnet1)" >> ~/.bash_profile && source ~/.bash_profile
echo "export psubnet2=$(cat ~/prsubnet2)" >> ~/.bash_profile && source ~/.bash_profile
echo "export psubnet3=$(cat ~/prsubnet3)" >> ~/.bash_profile && source ~/.bash_profile
echo "export networkcidr=$(cat ~/vpcidr)" >> ~/.bash_profile && source ~/.bash_profile
echo "export mastertype=t2.medium" >> ~/.bash_profile && source ~/.bash_profile
echo "export nodetype=t2.medium" >> ~/.bash_profile && source ~/.bash_profile
echo "export ami=ami-09a4a9ce71ff3f20b" >> ~/.bash_profile && source ~/.bash_profile
echo "export utsubnet1=$(cat ~/psubnet1)" >> ~/.bash_profile && source ~/.bash_profile
echo "export utsubnet2=$(cat ~/psubnet2)" >> ~/.bash_profile && source ~/.bash_profile
echo "export utsubnet3=$(cat ~/psubnet3)" >> ~/.bash_profile && source ~/.bash_profile

#Delete unwanted files
sudo rm -rf ~/prsubnet1
sudo rm -rf ~/prsubnet2
sudo rm -rf ~/prsubnet3
sudo rm -rf ~/psubnet1
sudo rm -rf ~/psubnet2
sudo rm -rf ~/psubnet3
sudo rm -rf ~/vpcid
sudo rm -rf ~/vpcidr
#validate
echo $s3bucketname
echo $clientname
echo $envname
echo $KOPS_STATE_STORE
echo $KOPS_CLUSTER_NAME
echo $awsregion
echo $clusterkey
echo $networkstackname
echo $vpcid
echo $psubnet1
echo $psubnet2
echo $psubnet3
echo $networkcidr
echo $mastertype
echo $nodetype
echo $ami
echo $utsubnet1
echo $utsubnet2
echo $utsubnet3

echo "export PATH=$PATH:/usr/bin/kops" >> ~/.bashrc && source ~/.bashrc
echo "export PATH=$PATH:/usr/bin/kubectl" >> ~/.bashrc && source ~/.bashrc

