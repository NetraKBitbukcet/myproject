image: docker:19.03.0

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:19.03.0-dind

before_script:
  - docker info

# image: docker:18.09.7-dind


stages:
#   #  - prebuild
   - build
   - postbuild

# build1:
#   stage: build
#   script:
#     - docker build --network host -t my-docker-image .
#     - docker run my-docker-image /script/to/run/tests

# variables:
#   DOCKER_DRIVER: overlay2
  
# before_script:
#     - dockerd &
#     - sleep 5
#     - docker info
#     - printenv
#     - docker info
 

# # prebuild:
# #     stage: prebuild
# #     variables:
# #       NAME: development
# #     tags:
# #       - preproduction
# #     cache:
# #       untracked: true
# #     script:
# #       - branch_name=`echo "${CI_COMMIT_REF_NAME////}"`
# #       - source DynamicVariables/$branch_name.env
# #       - apk add curl
# #       - apk add bash bash-doc bash-completion
# #       - apk add util-linux pciutils usbutils coreutils binutils findutils grep
# #       - echo $AWS_ACCOUNT_ID
# #       - docker --version
# #       - script -q -c "docker info"
# #       # - docker run busybox nslookup google.com

#       # - $(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)


build:
    stage: build
    cache:
      untracked: true
    script:
      # - branch_name=`echo "${CI_COMMIT_REF_NAME////}"`
      # - source DynamicVariables/$branch_name.env
      # - sed -e "2iENV predikt_branch_name=$CI_COMMIT_REF_NAME" -i Dockerfile
      # - sed -e "2iENV predikt_commit_id=gitcommit-$CI_COMMIT_SHA" -i Dockerfile
      # - sed -e "1iFROM $AWS_ACCOUNT_ID.dkr.ecr.ap-southeast-2.amazonaws.com/base-image:php7-v2" -i $DOCKER_FILE
      - apk add --no-cache python3
      - apk add curl
      - apk add bash bash-doc bash-completion
      - apk add util-linux pciutils usbutils coreutils binutils findutils grep
      - echo Build completed on `date`
      - echo Pushing the Docker image.
      - curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
      - python3 get-pip.py
      - pip3 install awscli --upgrade
      - export PATH=$PATH:$HOME/.local/bin
      - mkdir ~/.aws
      - touch ~/.aws/config
      - echo [default] >> ~/.aws/config
      - echo [default] >> ~/.aws/credentials
      - echo AWS_ACCESS_KEY_ID = $AWS_ACCESS_KEY_ID >> ~/.aws/credentials
      - echo AWS_SECRET_ACCESS_KEY = $AWS_SECRET_ACCESS_KEY >> ~/.aws/credentials
      - echo AWS_DEFAULT_REGION = $AWS_DEFAULT_REGION >> ~/.aws/config
      - echo output = json >> ~/.aws/config
      - cat ~/.aws/config
      - cat ~/.aws/credentials
      - $(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
      - docker build --network host -t $IMAGE_REPO_NAME -f $DOCKER_FILE .
      - docker tag $IMAGE_REPO_NAME:latest $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_REPO_NAME:gitcommit-$CI_COMMIT_SHA
      - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_REPO_NAME:gitcommit-$CI_COMMIT_SHA
      - docker rmi $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_REPO_NAME:gitcommit-$CI_COMMIT_SHA --force


post-build:
    stage: postbuild
    only:
      - master
    cache:
      untracked: true
    script:
      - env
      # - branch_name=`echo "${CI_COMMIT_REF_NAME////}"`
      # - source DynamicVariables/$branch_name.env
      - apk add --no-cache python3
      - apk add curl
      - apk add bash bash-doc bash-completion
      - apk add util-linux pciutils usbutils coreutils binutils findutils grep
      - echo Build completed on `date`
      - echo Pushing the Docker image.
      - curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
      - python3 get-pip.py
      - pip3 install awscli --upgrade
      - export PATH=$PATH:$HOME/.local/bin
      - curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/linux/amd64/aws-iam-authenticator
      - chmod +x ./aws-iam-authenticator
      - cp ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator
      - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
      - chmod +x ./kubectl
      - mv ./kubectl /usr/local/bin/kubectl
      - mkdir ~/.aws
      - touch ~/.aws/config
      - echo [default] >> ~/.aws/config
      - echo [default] >> ~/.aws/credentials
      - echo AWS_ACCESS_KEY_ID = $AWS_ACCESS_KEY_ID >> ~/.aws/credentials
      - echo AWS_SECRET_ACCESS_KEY = $AWS_SECRET_ACCESS_KEY >> ~/.aws/credentials
      - echo AWS_DEFAULT_REGION = $AWS_DEFAULT_REGION >> ~/.aws/config
      - echo output = json >> ~/.aws/config
      - cat ~/.aws/config
      - cat ~/.aws/credentials
      - aws s3 ls
      - $(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
      - aws eks --region ap-southeast-2 update-kubeconfig --name $EKS_CLUSTER_NAME
      - kubectl set image deployment/$KUBE_DEPLOYMENT_NAME2 webapp=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_REPO_NAME:gitcommit-$CI_COMMIT_SHA