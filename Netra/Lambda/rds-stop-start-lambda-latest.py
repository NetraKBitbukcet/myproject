import boto3
import re
import os
from subprocess import call
from datetime import date, datetime
from time import gmtime, strftime


Schedule_Format = re.compile('UPTIME-.{7}-\d{4}-\d{4}')


def determine_action(current_state, Automation_Schedule_Opt_Out, Automation_Schedule_UTC):
    resource_info = []
    no_action = False
    start = False
    stop = False

    today = date.today()
    weekday_num = today.weekday()
    weekday = today.strftime("%A")
    resource_info.append(weekday)
    current_time = strftime("%H%M", gmtime())

    if Automation_Schedule_Opt_Out.upper() == 'YES':
        print "Opting out of Automation Schedule"
        resource_info.append(
            "Opting out of Automation Schedule")
        no_action = True
    else:
        resource_info.append(current_state)
        if Automation_Schedule_UTC == 'UPTIME-ALWAYSON' and current_state == 'stopped':
            print "Automation_Schedule_UTC is %s and current_state is %s, starting the instance" % (
                Automation_Schedule_UTC, current_state)
            resource_info.append("Automation_Schedule_UTC is %s and current_state is %s, starting the instance" % (
                Automation_Schedule_UTC, current_state))
            start = True
        elif Automation_Schedule_UTC == 'UPTIME-ALWAYSON' and current_state == 'available':
            print "Automation_Schedule_UTC is %s and current_state is %s, nothing to be done" % (
                Automation_Schedule_UTC, current_state)
            resource_info.append("Automation_Schedule_UTC is %s and current_state is %s, nothing to be done" % (
                Automation_Schedule_UTC, current_state))
            no_action = True
        elif Automation_Schedule_UTC == 'UPTIME-ALWAYSOFF' and current_state == 'stopped':
            print "Automation_Schedule_UTC is %s and current_state is %s, nothing to be done" % (
                Automation_Schedule_UTC, current_state)
            resource_info.append("Automation_Schedule_UTC is %s and current_state is %s, nothing to be done" % (
                Automation_Schedule_UTC, current_state))
            no_action = True
        elif Automation_Schedule_UTC == 'UPTIME-ALWAYSOFF' and current_state == 'available':
            print "Automation_Schedule_UTC is %s and current_state is %s, stopping the instance" % (
                Automation_Schedule_UTC, current_state)
            resource_info.append("Automation_Schedule_UTC is %s and current_state is %s, stopping the instance" % (
                Automation_Schedule_UTC, current_state))
            stop = True
        elif Schedule_Format.match(Automation_Schedule_UTC) is not None:
            Schedule_Split = Automation_Schedule_UTC.split("-")
            days_array = Schedule_Split[1]
            start_time = Schedule_Split[2]
            end_time = Schedule_Split[3]
            exp_days_array = [
                'M', 'T', 'W', 'R', 'F', 'S', 'U']
            if start_time > '2359' or end_time > '2359':
                print "Start time %s or end time %s values should be between 0 to 2359, nothing to do" % (
                    start_time, end_time)
                resource_info.append(
                    "Start time %s or end time %s values should be between 0 to 2359, nothing to do" % (start_time, end_time))
                no_action = True
            else:
                print "time values are correct"
                if (days_array[weekday_num].upper() == 'X' or days_array[weekday_num].upper() != exp_days_array[weekday_num]) and current_state == 'available':
                    print "No action to be taken on day %s but instance is currently in available state. Will check start and end times" % exp_days_array[
                        weekday_num]
                    if start_time < end_time:
                        if current_state == 'available' and (('2400' > current_time > end_time) or (start_time > current_time > '0000')):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, stopping the instance" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, stopping the instance" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            stop = True
                        elif current_state == 'available' and (end_time > current_time > start_time):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            no_action = True
                    elif end_time < start_time:
                        if current_state == 'available' and (start_time > current_time > end_time):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, stopping the instance" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, stopping the instance" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            stop = True
                        elif current_state == 'available' and (('2400' > current_time > start_time) or (end_time > current_time > '0000')):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            no_action = True
                elif (days_array[weekday_num].upper() == 'X' or days_array[weekday_num].upper() != exp_days_array[weekday_num]) and current_state == 'stopped':
                    prevday_num = weekday_num-1
                    if days_array[prevday_num].upper() == exp_days_array[prevday_num]:
                        if start_time < end_time:
                            if current_state == 'stopped' and (end_time > current_time > start_time):
                                print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, starting the instance since previous day values is %s" % (
                                    Automation_Schedule_UTC, current_state, current_time, days_array[prevday_num])
                                resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, starting the instance since previous day values is %s" % (
                                    Automation_Schedule_UTC, current_state, current_time, days_array[prevday_num]))
                                start = True
                            elif current_state == 'stopped' and (('2400' > current_time > end_time) or (start_time > current_time > '0000')):
                                print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, taking no action" % (
                                    Automation_Schedule_UTC, current_state, current_time)
                                resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, taking no action" % (
                                    Automation_Schedule_UTC, current_state, current_time))
                                no_action = True
                        elif end_time < start_time:
                            if current_state == 'stopped' and (('2400' > current_time > start_time) or (end_time > current_time > '0000')):
                                print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, starting the instance since previous day values is %s" % (
                                    Automation_Schedule_UTC, current_state, current_time, days_array[prevday_num])
                                resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, starting the instance since previous day values is %s" % (
                                    Automation_Schedule_UTC, current_state, current_time, days_array[prevday_num]))
                                start = True
                            elif current_state == 'stopped' and (start_time > current_time > end_time):
                                print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, taking no action" % (
                                    Automation_Schedule_UTC, current_state, current_time)
                                resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, taking no action" % (
                                    Automation_Schedule_UTC, current_state, current_time))
                                no_action = True
                    else:
                        print days_array[weekday_num].upper(
                        ), exp_days_array[weekday_num], current_state
                        print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, no action since day is %s, expected value is %s" % (
                            Automation_Schedule_UTC, current_state, current_time, days_array[weekday_num].upper(), exp_days_array[weekday_num].upper())
                        resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, no action since day is %s, expected value is %s" % (
                            Automation_Schedule_UTC, current_state, current_time, days_array[weekday_num].upper(), exp_days_array[weekday_num].upper()))
                        no_action = True
                else:
                    if start_time < end_time:
                        if current_state == 'available' and (('2400' > current_time > end_time) or (start_time > current_time > '0000')):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, stopping the instance" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, stopping the instance" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            stop = True
                        elif current_state == 'available' and (end_time > current_time > start_time):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            no_action = True
                        elif current_state == 'stopped' and (end_time > current_time > start_time):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, starting the instance" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, starting the instance" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            start = True
                        elif current_state == 'stopped' and (('2400' > current_time > end_time) or (start_time > current_time > '0000')):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            no_action = True
                    elif end_time < start_time:
                        if current_state == 'available' and (start_time > current_time > end_time):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, stopping the instance" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, stopping the instance" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            stop = True
                        elif current_state == 'available' and (('2400' > current_time > start_time) or (end_time > current_time > '0000')):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            no_action = True
                        elif current_state == 'stopped' and (('2400' > current_time > start_time) or (end_time > current_time > '0000')):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, starting the instance" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, starting the instance" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            start = True
                        elif current_state == 'stopped' and (start_time > current_time > end_time):
                            print "Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time)
                            resource_info.append("Automation_Schedule_UTC is %s, current_state is %s, current time is %s, nothing to do" % (
                                Automation_Schedule_UTC, current_state, current_time))
                            no_action = True
        else:
            print "Automation_Schedule_UTC is %s which is not in correct format\n     \
  Correct Format should be like UPTIME-MTWTFXX-0700-1900 or UPTIME-ALWAYSON/UPTIME-ALWAYSOFF" % Automation_Schedule_UTC
            resource_info.append("Automation_Schedule_UTC is %s which is not in correct format\n     \
  Correct Format should be like UPTIME-MTWTFXX-0700-1900 or UPTIME-ALWAYSON/UPTIME-ALWAYSOFF" % Automation_Schedule_UTC)
            no_action = True
    return (resource_info, start, stop, no_action)


def get_schedule_tags(tagslist):
    Automation_Schedule_Opt_Out = ''
    Automation_Schedule_UTC = ''
    req_tag_keys = ['Automation_Schedule_Opt_Out', 'Automation_Schedule_UTC']
    for tag in req_tag_keys:
        tagkey_found = 0
        for i in range(len(tagslist['TagList'])):
            if tag == tagslist['TagList'][i]['Key']:
                tagkey_found = 1
                if not tagslist['TagList'][i]['Value']:
                    tagvalue_found = 0

                else:
                    if tag == 'Automation_Schedule_Opt_Out':
                        Automation_Schedule_Opt_Out = tagslist['TagList'][i]['Value']
                    elif tag == 'Automation_Schedule_UTC':
                        Automation_Schedule_UTC = tagslist['TagList'][i]['Value']
                    tagvalue_found = 1
                    break
            else:
                tagkey_found = 0
    return Automation_Schedule_Opt_Out, Automation_Schedule_UTC


def get_db_instance_arrays(rds):
    start_array = []
    stop_array = []
    no_action = []
    instances = rds.describe_db_instances()
    for instance in instances['DBInstances']:
        if 'DBClusterIdentifier' in instance and instance['DBClusterIdentifier']:
            continue
        rdsname = instance['DBInstanceIdentifier']
        rdsarn = instance['DBInstanceArn']
        dbinstancetype = instance['DBInstanceClass']
        dbstatus = instance['DBInstanceStatus']
        tagslist = rds.list_tags_for_resource(ResourceName=rdsarn, Filters=[])
        instance_info = []
        instance_info.append(rdsname)

        if dbstatus == 'available' or dbstatus == 'stopped':
            if not tagslist['TagList']:
                print "No tags found for the instance id %s" % rdsname
                instance_info.append("No tags found for the instance id")
                no_action.append(instance_info)
            else:
                Automation_Schedule_Opt_Out, Automation_Schedule_UTC = get_schedule_tags(
                    tagslist)
                if Automation_Schedule_UTC and Automation_Schedule_Opt_Out:
                    print "appr key and values exist for instance %s" % rdsname
                    print rdsname, dbinstancetype, dbstatus, Automation_Schedule_Opt_Out, Automation_Schedule_UTC
                    current_state = instance['DBInstanceStatus']
                    (resource_info, start, stop, do_nothing) = determine_action(
                        current_state, Automation_Schedule_Opt_Out, Automation_Schedule_UTC)
                    instance_info = instance_info + resource_info
                    if start:
                        start_array.append(instance_info)
                    elif stop:
                        stop_array.append(instance_info)
                    else:
                        no_action.append(instance_info)
                else:
                    print "Appropriate Key or Value for tags 'Automation_Schedule_Opt_Out' or 'Automation_Schedule_UTC' not found for instance %s" % rdsname
                    instance_info.append(
                        "Appropriate Key or Value for tags 'Automation_Schedule_Opt_Out' or 'Automation_Schedule_UTC' not found for instance %s" % rdsname)
                    no_action.append(instance_info)
        else:
            print "DB Instance is not in available or stopped state"

    return start_array, stop_array, no_action


def get_db_cluster_arrays(rds):
    start_array = []
    stop_array = []
    no_action = []
    clusters = rds.describe_db_clusters()
    for cluster in clusters['DBClusters']:
        rdsname = cluster['DBClusterIdentifier']
        rdsarn = cluster['DBClusterArn']
        dbengine = cluster['Engine']
        dbstatus = cluster['Status']
        tagslist = rds.list_tags_for_resource(ResourceName=rdsarn, Filters=[])
        cluster_info = []
        cluster_info.append(rdsname)

        if dbstatus == 'available' or dbstatus == 'stopped':
            if not tagslist['TagList']:
                print "No tags found for the cluster id %s" % rdsname
                cluster_info.append("No tags found for the cluster id")
                no_action.append(cluster_info)
            else:
                Automation_Schedule_Opt_Out, Automation_Schedule_UTC = get_schedule_tags(
                    tagslist)
                if Automation_Schedule_UTC and Automation_Schedule_Opt_Out:
                    print "Automation key and values exist for cluster %s" % rdsname
                    print rdsname, dbengine, dbstatus, Automation_Schedule_Opt_Out, Automation_Schedule_UTC
                    (resource_info, start, stop, do_nothing) = determine_action(
                        dbstatus, Automation_Schedule_Opt_Out, Automation_Schedule_UTC)
                    cluster_info = cluster_info + resource_info
                    if start:
                        start_array.append(cluster_info)
                    elif stop:
                        stop_array.append(cluster_info)
                    else:
                        no_action.append(cluster_info)
                else:
                    print "Appropriate Key or Value for tags 'Automation_Schedule_Opt_Out' or 'Automation_Schedule_UTC' not found for cluster %s" % rdsname
                    cluster_info.append(
                        "Appropriate Key or Value for tags 'Automation_Schedule_Opt_Out' or 'Automation_Schedule_UTC' not found for cluster %s" % rdsname)
                    no_action.append(cluster_info)
        else:
            print "DB cluster is not in available or stopped state"

    return start_array, stop_array, no_action


def lambda_handler(event, context):
    sts_client = boto3.client('sts')
    RoleArnSub = ''.join(['arn:aws:iam::', '%s', ':role/', '%s']
                         ) % (event['account_ID'], event['role_name'])
    # RoleArnSub='arn:aws:iam::959515647555:role/CrossAccount-Tagging-Trust'
    # RoleArnSub='arn:aws:iam::959515647555:role/Operation-Automation'
    print RoleArnSub

    assumedRoleObject = sts_client.assume_role(
        RoleArn=RoleArnSub,
        RoleSessionName="AssumeRoleSession1"
    )

    credentials = assumedRoleObject['Credentials']
    rds = boto3.client('rds', 'ap-southeast-1', aws_access_key_id=credentials['AccessKeyId'],
                       aws_secret_access_key=credentials['SecretAccessKey'], aws_session_token=credentials['SessionToken'])
    # rds = boto3.client('rds', 'ap-southeast-1')
    current_time = strftime("%H%M", gmtime())
    s3_file_name = "start_stop_logs_%s_%s.txt" % (
        datetime.utcnow().strftime("%Y_%m_%d"), current_time)
    s3_file_path = "/tmp/%s" % s3_file_name

    complete_array = []

    (start_instance_array, stop_instance_array,
     no_action_instance) = get_db_instance_arrays(rds)

    (start_cluster_array, stop_cluster_array,
     no_action_cluster) = get_db_cluster_arrays(rds)

    print "printing out final array:"
    complete_array.append("printing out final array:")
    # processing instances
    if len(start_instance_array) > 0:
        print "start_array for instances is %s" % start_instance_array
        complete_array.append("start_array for instances is:")
        complete_array.append(start_instance_array)
        start_ids = []
        for i in range(len(start_instance_array)):
            print "db names for start"
            startinstance = start_instance_array[i][0]
            print startinstance

            starting = rds.start_db_instance(
                DBInstanceIdentifier=startinstance)
        print starting
    else:
        print "no instances found to be started"
        complete_array.append("no instances found to be started")
    if len(stop_instance_array) > 0:
        print "stop_array for instances is %s" % stop_instance_array
        complete_array.append("stop array for instances is:")
        complete_array.append(stop_instance_array)
        stop_ids = []
        for i in range(len(stop_instance_array)):
            print stop_instance_array[i][0]
            stoptinstance = stop_instance_array[i][0]

            stopping = rds.stop_db_instance(DBInstanceIdentifier=stoptinstance)
        print stopping
    else:
        print "no instances found to be stopped"
        complete_array.append("no instances found to be stopped")
    if len(no_action_instance) > 0:
        print "no_action array for instances is %s" % no_action_instance
        complete_array.append("no action done on instances:")
        complete_array.append(no_action_instance)
    else:
        print "no instances found for no action to be taken"
        complete_array.append("no instances for 'no action'")

    # processing clusters
    if len(start_cluster_array) > 0:
        print "start_array for cluster is %s" % start_cluster_array
        complete_array.append("start_array for clusters is:")
        complete_array.append(start_cluster_array)
        for i in range(len(start_cluster_array)):
            print "db names for start"
            db_cluster_identifier = start_cluster_array[i][0]
            print db_cluster_identifier
            starting = rds.start_db_cluster(
                DBClusterIdentifier=db_cluster_identifier)
        print starting
    else:
        print "no clusters found to be started"
        complete_array.append("no clusters found to be started")
    if len(stop_cluster_array) > 0:
        print "stop_array for clusters is %s" % stop_cluster_array
        complete_array.append("stop array for clusters is:")
        complete_array.append(stop_cluster_array)
        for i in range(len(stop_cluster_array)):
            print stop_cluster_array[i][0]
            db_cluster_identifier = stop_cluster_array[i][0]
            stopping = rds.stop_db_cluster(
                DBClusterIdentifier=db_cluster_identifier)
        print stopping
    else:
        print "no clusters found to be stopped"
        complete_array.append("no clusters found to be stopped")
    if len(no_action_cluster) > 0:
        print "no_action array for clusters is %s" % no_action_cluster
        complete_array.append("no action done on clusters:")
        complete_array.append(no_action_cluster)
    else:
        print "no clusters found for no action to be taken"
        complete_array.append("no clusters for 'no action'")

    f = open(s3_file_path, 'w')
    for item in complete_array:
        f.write("%s\n" % item)
    f.close()

    bucket_name = event['bucket_name']
    # bucket_name='coe-magento'

    f = open(s3_file_path, 'rb')
    s3_client = boto3.client('s3', aws_access_key_id=credentials['AccessKeyId'],
                             aws_secret_access_key=credentials['SecretAccessKey'], aws_session_token=credentials['SessionToken'])
    # s3_client = boto3.client('s3')
    obj = s3_client.upload_file(s3_file_path, bucket_name, s3_file_name)
    f.close()

    print "file content is:"
    f = open(s3_file_path, 'r')
    print f.read()

    return {'output': complete_array}
