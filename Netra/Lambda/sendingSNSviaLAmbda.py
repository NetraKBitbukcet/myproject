import json
import boto3
#from datetime import datetime

sns_client = boto3.client('sns')
client = boto3.client('ec2')
def lambda_handler(event, context):
    response = client.describe_instances(
    Filters=[
        {
            'Name': 'tag:Monitoring',
            'Values': [
                'True',
            ]
        }
    ]
    )
    instance_id=response['Reservations'][0]['Instances'][0]['InstanceId']
    
    message = {
        "Alarm Name=": "Instance is running for more than 2 hours",
        "instanceid": instance_id
        
    }
    
    
    print(instance_id)
   
    sns_response = sns_client.publish(
    TopicArn='arn:aws:sns:us-east-1:744400539196:testing',
    Message=json.dumps(message),
    Subject='Alarm for Instance is running more than 2 hours'
    )        
