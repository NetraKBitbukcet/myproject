import boto3

import logging


#setup simple logging for INFO

logger = logging.getLogger()

logger.setLevel(logging.INFO)


ec2 = boto3.resource('ec2')

def lambda_handler(event, context):
   
    filters = [{

            'Name': 'tag:AutoOn',

            'Values': ['True']

        },

        {

            'Name': 'instance-state-name', 

            'Values': ['running']

        }

    ]

    

    #filter the instances

    instances = ec2.instances.filter(Filters=filters)


    #locate all running instances

    runningInstances = [instance.id for instance in instances]

    #print the instances for logging purposes

    #print RunningInstance 
    #make sure there are actually instances to Start up. 
#please change "-gt" with frater than symbol.   
    if len(runningInstances) > 0:

        #perform the stop

        stopUp = ec2.instances.filter(InstanceIds=runningInstances).stop()

        print(stopUp)

    else:

        print("Nothing to see here")
        
        
#RDS Stop script        
    rds = boto3.client('rds')
    lambdaFunc = boto3.client('lambda')

    print('Trying to get Environment variable')
    funcResponse = lambdaFunc.get_function_configuration(
        FunctionName='RDS-Stop'
    )
        
    DBinstance = funcResponse['Environment']['Variables']['DBInstanceName']
    print (DBinstance)
    print('Stoping RDS service for DBInstance : ' + DBinstance)
    
    response = rds.stop_db_instance(
        DBInstanceIdentifier=DBinstance
    )
    print('Success :: ') 

    
#################
# Add environment variable for RDS to identiy the instance name
# Key:DBInstanceName
# Vlaue: Database_name
    